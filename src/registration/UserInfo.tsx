import React from 'react';
import { TextField, DialogContent, DialogTitle, DialogActions, Button, Dialog } from '@material-ui/core';

interface UserInfoProps {
  registrationData: any;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handlePreviousStep: () => void;
  handleNextStep: () => void;
}

const UserInfo: React.FC<UserInfoProps> = ({
  registrationData,
  handleInputChange,
  handlePreviousStep,
  handleNextStep,
}) => {

  const isValidFields = () => {
    return (
      registrationData.firstName &&
      registrationData.lastName &&
      registrationData.phoneNumber &&
      registrationData.houseNumber &&
      registrationData.street &&
      registrationData.city &&
      registrationData.country &&
      registrationData.pinCode
    );
  };
  return (
    <Dialog open = {true}>

    <div className='main-container'>
      <DialogTitle>Step 2: Personal Information</DialogTitle>
      <DialogContent>
        <div className='second-section'>
          <TextField
            type="text"
            name="firstName"
            label="First Name"
            value={registrationData.firstName}
            onChange={handleInputChange}
            required
            fullWidth
          />
          <TextField
            type="text"
            name="lastName"
            label="Last Name"
            value={registrationData.lastName}
            onChange={handleInputChange}
            required
            fullWidth
          />
          <TextField
            type="tel"
            name="phoneNumber"
            label="Phone Number"
            value={registrationData.phoneNumber}
            onChange={handleInputChange}
            required
            fullWidth
          />
          <TextField
            type="text"
            name="houseNumber"
            label="House Number"
            value={registrationData.houseNumber}
            onChange={handleInputChange}
            required
            fullWidth
          />
          <TextField
            type="text"
            name="street"
            label="Street"
            value={registrationData.street}
            onChange={handleInputChange}
            required
            fullWidth
          />
          <TextField
            type="text"
            name="city"
            label="City"
            value={registrationData.city}
            onChange={handleInputChange}
            required
            fullWidth
          />
          <TextField
            type="text"
            name="country"
            label="Country"
            value={registrationData.country}
            onChange={handleInputChange}
            required
            fullWidth
          />
          <TextField
            type="text"
            name="pinCode"
            label="Pin Code"
            value={registrationData.pinCode}
            onChange={handleInputChange}
            required
            fullWidth
          />
        </div>
      </DialogContent>
      <DialogActions>
        <Button disabled={!isValidFields()}  onClick={handleNextStep}>Next</Button>
      </DialogActions>
    </div>
    </Dialog>
  );
}

export default UserInfo;
