import React, { useState } from 'react';
import RegistrationData from './interfaces/RegistrationInterface';
import PasswordInfo from './PasswordInfo';
import UserInfo from './UserInfo';
import CompanyInfo from './CompanyInfo';
import { callUserSaveApi } from './RegistrationApi';
import { useNavigate } from 'react-router-dom';
import GenericSnackbar from '../utils/GenericSnackbar';

const RegistrationForm: React.FC = () => {
  const navigate = useNavigate();
  const [step, setStep] = useState(1);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState('');
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const [registrationData, setRegistrationData] = useState<RegistrationData>({
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    phoneNumber: '',
    companyId: 0,
    isAdmin: false,
    isOwner: false,
    houseNumber: '',
    street: '',
    city: '',
    country: '',
    pinCode: '',
    position: '',
    maximumWorkingHours: '',
    type: '',
    salaryPerMonth: '',
    managerId: 0,
    isManager: false,
    key:'',
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setRegistrationData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleConfirmPasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setConfirmPassword(e.target.value);
  };

  const handleNextStep = () => {
    setStep((prevStep) => prevStep + 1);
  };

  const handlePreviousStep = () => {
    setStep((prevStep) => prevStep - 1);
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    console.log('registrationData', registrationData);
    try {
      const response = await callUserSaveApi(registrationData);

      if (response.statusCode !== 200) {
        setSnackbarMessage(response.message);
        setSnackbarOpen(true);
      } else {
        setSnackbarMessage("Successfully registered! please login");
      setSnackbarOpen(true);
      setRegistrationSuccess(true); // Set the registration success state
      setTimeout(() => {
        navigate('/'); 
      }, 3000);
    }
      
    } catch (error) {
      setSnackbarMessage('An error occurred');
      setSnackbarOpen(true);
    }
  };

  const handleTogglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const handleToggleConfirmPasswordVisibility = () => {
    setShowConfirmPassword((prevShowConfirmPassword) => !prevShowConfirmPassword);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const renderStep = () => {
    switch (step) {
      case 1:
        return (
          <PasswordInfo
            registrationData={registrationData}
            handleInputChange={handleInputChange}
            showPassword={showPassword}
            handleTogglePasswordVisibility={handleTogglePasswordVisibility}
            showConfirmPassword={showConfirmPassword}
            handleToggleConfirmPasswordVisibility={handleToggleConfirmPasswordVisibility}
            confirmPassword={confirmPassword}
            handleConfirmPasswordChange={handleConfirmPasswordChange}
            handleNextStep={handleNextStep}
          />
        );
      case 2:
        return (
          <UserInfo
            registrationData={registrationData}
            handleInputChange={handleInputChange}
            handlePreviousStep={handlePreviousStep}
            handleNextStep={handleNextStep}
          />
        );
      case 3:
        return (
          <><CompanyInfo
            registrationData={registrationData}
            handleInputChange={handleInputChange}
            handlePreviousStep={handlePreviousStep}
            handleSubmit={handleSubmit} />

            <GenericSnackbar
              message={snackbarMessage}
              severity={registrationSuccess ? "success" : "error"}
              onClose={handleSnackbarClose}
              open={snackbarOpen}
              key={snackbarMessage} /></>
        );
      default:
        return null;
    }
  };

  return (
    <div className="registration-form-container">
    <form onSubmit={handleSubmit}>
      {renderStep()}
    </form>
  </div>
  );
};

export default RegistrationForm;
