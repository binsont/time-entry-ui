interface RegistrationData {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    isAdmin: boolean;
    isOwner: boolean;
    houseNumber: string;
    street: string;
    city: string;
    country: string;
    pinCode: string;
    position: string;
    maximumWorkingHours: string;
    type: string;
    salaryPerMonth: string;
    managerId: number;
    companyId:number;
    isManager: boolean;
    key : string;
  }
  
  export default RegistrationData;
