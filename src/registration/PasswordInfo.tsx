import React, { useState } from 'react';
import './RegistrationForm.css';
import { TextField, IconButton, InputAdornment, DialogContent, DialogTitle, DialogActions, Button, Dialog } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import RegistrationData from './interfaces/RegistrationInterface';

const PasswordInfo = ({
  registrationData,
  handleInputChange,
  showPassword,
  handleTogglePasswordVisibility,
  showConfirmPassword,
  handleToggleConfirmPasswordVisibility,
  confirmPassword,
  handleConfirmPasswordChange,
  handleNextStep,
}: {
  registrationData: RegistrationData;
  handleInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  showPassword: boolean;
  handleTogglePasswordVisibility: () => void;
  showConfirmPassword: boolean;
  handleToggleConfirmPasswordVisibility: () => void;
  confirmPassword: string;
  handleConfirmPasswordChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleNextStep : () => void;
}) => {

  const [isEmailValid, setEmailValid] = useState(false);
  const [isPasswordValid, setPasswordValid] = useState(false);
  const [isConfirmPasswordValid, setConfirmPasswordValid] = useState(false);
  const [errors, setErrors] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  });
  const validateEmail = (email: string) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setErrors((prevErrors) => ({ ...prevErrors, email: 'Invalid email address' }));
      setEmailValid(false)
    } else {
      setErrors((prevErrors) => ({ ...prevErrors, email: '' }));
      setEmailValid(true)

    }
  };

  const validatePassword = (password: string) => {
    if (password.length < 8) {
      setPasswordValid(false)
      setErrors((prevErrors) => ({
        ...prevErrors,
        password: 'Password must be at least 8 characters long',
      }));
    } else {
      setPasswordValid(true)
      setErrors((prevErrors) => ({ ...prevErrors, password: '' }));
    }
  };

  const validateConfirmPassword = (confirmPassword: string) => {
    if (confirmPassword !== registrationData.password) {
      setConfirmPasswordValid(false)
      setErrors((prevErrors) => ({ ...prevErrors, confirmPassword: 'Passwords do not match' }));
    } else {
      setConfirmPasswordValid(true)
      setErrors((prevErrors) => ({ ...prevErrors, confirmPassword: '' }));
    }
  };

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    handleInputChange(e);
    validateEmail(value);
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    handleInputChange(e);
    validatePassword(value);
  };

  const handleConfirmPasswordChanging = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    handleConfirmPasswordChange(e);
    validateConfirmPassword(value);
  };

  return (
    <Dialog open={true}>
    <div className='main-container'>
      <DialogTitle>Step 1: Email and Password Setting</DialogTitle>
      <DialogContent>
        <TextField
          type="email"
          name="email"
          label="Email"
          placeholder="Enter your email"
          value={registrationData.email}
          onChange={handleEmailChange}
          required
          fullWidth
          error={!!errors.email}
          helperText={errors.email}
        />
        <TextField
          type={showPassword ? 'text' : 'password'}
          name="password"
          label="Password"
          placeholder="Enter your password"
          value={registrationData.password}
          onChange={handlePasswordChange}
          required
          fullWidth
          error={!!errors.password}
          helperText={errors.password}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={handleTogglePasswordVisibility}>
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          type={showConfirmPassword ? 'text' : 'password'}
          name="confirmPassword"
          label="Confirm Password"
          placeholder="Confirm your password"
          value={confirmPassword}
          onChange={handleConfirmPasswordChanging}
          required
          fullWidth
          error={!!errors.confirmPassword}
          helperText={errors.confirmPassword}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={handleToggleConfirmPasswordVisibility}>
                  {showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button disabled={!isEmailValid || !isPasswordValid || !isConfirmPasswordValid} onClick={handleNextStep}>Next</Button>
      </DialogActions>
    </div>
    </Dialog>
  );
}

export default PasswordInfo;
