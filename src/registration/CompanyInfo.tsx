import React, { useEffect, useState } from 'react';
import './RegistrationForm.css';
import { callGetCompanies, callGetManagers } from './RegistrationApi';
import { TextField, DialogContent, DialogTitle, DialogActions, Button, Dialog, MenuItem, FormControl, Select, InputLabel } from '@material-ui/core';
import { Autocomplete, AutocompleteInputChangeReason } from '@mui/material';
import { option } from 'yargs';

interface CompanyInfoProps {
    registrationData: any;
    handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    handlePreviousStep: () => void;
    handleSubmit: (event: React.FormEvent) => void;
}

const CompanyInfo: React.FC<CompanyInfoProps> = ({
    registrationData,
    handleInputChange,
    handleSubmit,
}) => {
    const [role, setRole] = useState('USER');
    const [companies, setCompanies] = useState<any[]>([]);
    const [managers, setManagers] = useState<any[]>([]);
    const [selectedCompanyId, setSelectedCompanyId] = useState<any>();
    const [selectedManagerId, setSelectedManagerId] = useState<any>();


    const fetchCompanies = async (search: string) => {
        try {
            const response = await callGetCompanies(search);
            setCompanies(response.data)
            console.log("after search", companies)
        } catch (error) {
            console.log(error);
        }
    };

    const fetchManagers = async (companyId: number) => {
        setManagers([])
        try {
            const response = await callGetManagers(companyId);
            setManagers(response.data)
            console.log("managers result", managers)
        } catch (error) {
            console.log(error);
        }
    };


    useEffect(() => {
        fetchCompanies('');
    }, []);

    useEffect(() => {
        if (selectedCompanyId) {
            fetchManagers(selectedCompanyId);
        }
    }, [selectedCompanyId]);


    const isValidCompanyFields = () => {
        return (
            registrationData.companyId &&
            registrationData.position &&
            registrationData.maximumWorkingHours &&
            registrationData.type &&
            registrationData.salaryPerMonth &&
            registrationData.managerId &&
            role &&
            registrationData.key
        );
    };
    const handleCompanySearch = (event: React.ChangeEvent<{}>, value: string, reason: AutocompleteInputChangeReason) => {
        if (reason === 'input') {
            setCompanies([])
            fetchCompanies(value);
        }
    };

    const handleManagerDropdownClick = () => {
        if (selectedCompanyId) {
            fetchManagers(selectedCompanyId);
        }
    };

    const handleManagerDropdownClose = () => {
       setManagers([])
    };


    const handleCompanySelect = (event: React.ChangeEvent<{}>, value1: any) => {
        const companyId = value1?.id;
        setSelectedCompanyId(companyId);
        registrationData.companyId = companyId; 
    };

    const handleManagerNameSelect = (event: React.ChangeEvent<{}>, value1: any) => {
        const managerId = value1?.id;
        setSelectedManagerId(managerId);
        registrationData.managerId = managerId;    };


        const handleRoleSelection = (e: { target: { value: any } }) => {
            const selectedRole = e.target.value;
            setRole(selectedRole);
            if (selectedRole === 'OWNER') {
              registrationData.isOwner = true;
              registrationData.isAdmin = false;
              registrationData.isManager = false;
            } else if (selectedRole === 'ADMIN') {
              registrationData.isAdmin = true;
              registrationData.isOwner = false;
              registrationData.isManager = false;
            } else if (selectedRole === 'MANAGER') {
              registrationData.isManager = true;
              registrationData.isOwner = false;
              registrationData.isAdmin = false;
            } else {
              registrationData.isManager = false;
              registrationData.isOwner = false;
              registrationData.isAdmin = false;
            }
          };
          
    return (
        <Dialog open={true}>
            <div className='main-container'>
                <DialogTitle>Step 3: Company Information</DialogTitle>
                <DialogContent>
                    <div>
                        <FormControl fullWidth required>
                            <Autocomplete
                                options={companies}
                                getOptionLabel={(option: any) => option.contactEmail}
                                onChange={handleCompanySelect}
                                onInputChange={handleCompanySearch}
                                renderInput={(params: any) => (
                                    <TextField
                                        {...params}
                                        label="Select Company id"
                                        fullWidth
                                        required
                                    />
                                )}
                            />
                        </FormControl>
                        <TextField
                            type="text"
                            name="position"
                            placeholder="Position"
                            value={registrationData.position}
                            onChange={handleInputChange}
                            fullWidth
                            required
                        />
                        <TextField
                            type="text"
                            name="maximumWorkingHours"
                            placeholder="Maximum Working Hours"
                            value={registrationData.maximumWorkingHours}
                            onChange={handleInputChange}
                            fullWidth
                            required
                        />
                        <TextField
                            type="text"
                            name="type"
                            placeholder="Type"
                            value={registrationData.type}
                            onChange={handleInputChange}
                            fullWidth
                            required
                        />
                        <TextField
                            type="text"
                            name="salaryPerMonth"
                            placeholder="Salary Per Month"
                            onChange={handleInputChange}
                            fullWidth
                            required
                        />
                        <FormControl fullWidth required>
                            <Autocomplete
                                onClose={handleManagerDropdownClose}
                                options={managers}
                                getOptionLabel={(option: any) => option.firstName}
                                onChange={handleManagerNameSelect}
                                onOpen={handleManagerDropdownClick}
                                renderInput={(params: any) => (
                                    <TextField
                                        {...params}
                                        label="Select your manager name"
                                        fullWidth
                                        required
                                    />
                                )}
                            />
                        </FormControl>
                        <FormControl fullWidth required>
                            <InputLabel>Select Role</InputLabel>
                            <Select defaultValue={'USER'} onChange={handleRoleSelection}>
                                <option value="OWNER">OWNER</option>
                                <option value="ADMIN">ADMIN</option>
                                <option value="MANAGER">MANAGER</option>
                                <option value="USER">USER</option>
                            </Select>
                        </FormControl>
                        {role && (
                            <TextField
                                type="text"
                                name="key"
                                label={`${role.toUpperCase()} SECRET KEY`}
                                value={registrationData.key}
                                onChange={handleInputChange}
                                fullWidth
                                required
                            />
                        )}
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button type="submit"
                        disabled={!isValidCompanyFields()}
                        onClick={handleSubmit}>Register</Button>
                </DialogActions>
            </div>
        </Dialog>
    );
}

export default CompanyInfo;


