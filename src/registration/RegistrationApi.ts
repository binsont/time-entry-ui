import callApi from "../api-client/ApiClient";
import RegistrationData from "./interfaces/RegistrationInterface";

export const callGetCompanies = async (search: string) => {
  try {
    const response = await callApi<{
      data: any;
      message: any;
      statusCode: number;
      token: string;
    }>('/company/search', {
      method: 'GET',
      queryParams: {
        search: search,
      },
    }, false); // Set includeAuthToken to false
    return response;
  } catch (error) {
    throw error;
  }
};

export const callGetManagers = async (id: number) => {
    try {
      const response = await callApi<{
        data: any;
        message: any;
        statusCode: number;
        token: string;
      }>(`/user/managers/${id}`, {
        method: 'GET',
        
      }, false); // Set includeAuthToken to false
      return response;
    } catch (error) {
      throw error;
    }
  };

  export const callUserSaveApi = async (data: RegistrationData) => {
    try {
      const response = await callApi<{
        message: any;
        statusCode: number; token: string 
  }>('/user/save', {
        method: 'POST',
        body:data,
      }, false); // Set includeAuthToken to false
      console.log('test response', response);
      return response;
    } catch (error) {
      throw error;
    }
  };