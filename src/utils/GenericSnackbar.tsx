import React, { useState, useEffect } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@mui/material/Alert';

interface SnackbarProps {
  message: string;
  severity: 'success' | 'error' | 'warning' | 'info';
  onClose: () => void;
  open: boolean;
  key: string | number; // Added the key prop for re-rendering
}

const GenericSnackbar: React.FC<SnackbarProps> = ({ message, severity, onClose, open, key }) => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  const handleClose = (_event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setIsOpen(false);
    onClose();
  };

  return (
    <Snackbar key={key} open={isOpen} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity={severity} elevation={6} variant="filled">
        {message}
      </Alert>
    </Snackbar>
  );
};

export default GenericSnackbar;
