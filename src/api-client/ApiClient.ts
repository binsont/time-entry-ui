import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

interface RequestOptions extends AxiosRequestConfig {
  pathVariables?: Record<string, string>;
  queryParams?: Record<string, string | number | boolean>;
  body?: any;
}

const API_BASE_URL = 'http://localhost:8082/time-entry';

const callApi = async <T>(url: string, options: RequestOptions = {}, includeAuthToken = true): Promise<T> => {
  const { pathVariables, queryParams, body, ...axiosConfig } = options;

  // Add authorization header if available and includeAuthToken is true
  if (includeAuthToken) {
    const authToken = 'YOUR_AUTH_TOKEN';
    if (authToken) {
      axiosConfig.headers = {
        ...axiosConfig.headers,
        Authorization: `Bearer ${authToken}`,
      };
    }
  }

  // Replace path variables in the URL
  let finalUrl = url;
  if (pathVariables) {
    Object.entries(pathVariables).forEach(([key, value]) => {
      finalUrl = finalUrl.replace(`:${key}`, value);
    });
  }

  // Perform the API request
  try {
    const response: AxiosResponse<T> = await axios.request<T>({
      url: `${API_BASE_URL}${finalUrl}`,
      params: queryParams,
      data: body,
      ...axiosConfig,
    });

    return response.data;
  } catch (error) {
    // Handle error
    throw error;
  }
};


export default callApi;