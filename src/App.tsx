import React from 'react';
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import Login from './login/Login';
import RegistrationForm from './registration/Registration';

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/signup" element={<RegistrationForm />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
