import React, { useState } from 'react';
import './Login.css';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { TextField, IconButton, InputAdornment } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { callLoginApi } from './LoginApi';
import GenericSnackbar from '../utils/GenericSnackbar';
import { Link } from 'react-router-dom';

const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleEmailChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
    setPassword(event.target.value);
  };

  const handleFormSubmit = async (event: { preventDefault: () => void; }) => {
    event.preventDefault();

    try {
      const response = await callLoginApi(email, password);

      if (response.statusCode !== 200) {
        setSnackbarMessage(response.message);
        setSnackbarOpen(true);
      } else {
        // Handle successful login here
      }
    } catch (error) {
      setSnackbarMessage('An error occurred');
      setSnackbarOpen(true);
    }
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };


  return (
    <div className="login-container">
      <div className="left-section">
        <h3 className="welcome-heading">Welcome To Time Entry</h3>
        <form className="login-form"  onSubmit={handleFormSubmit}>
          <div className="form-group">
            <label htmlFor="email" className="email">Email</label>
            <TextField
              type="email"
              id="email"
              name="email"
              placeholder="Enter your email"
              fullWidth
              value={email}
              onChange={handleEmailChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password" className="password">Password</label>
            <TextField
              type={showPassword ? 'text' : 'password'}
              id="password"
              name="password"
              placeholder="Enter your password"
              value={password}
              onChange={handlePasswordChange}
              fullWidth
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={togglePasswordVisibility}>
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <button type="submit" className="login-button">Login</button>
          <div className="forgot-password">
            <a className="forgot-password" href="#">Forgot Password?</a>
          </div>
          <div className="registration-link">
            Don't have an account? <Link to="/signup">Register here</Link>
          </div>
        </form>
      </div>
      <div className="line"></div>
      <div className="right-section">
        <Carousel showThumbs={true} className='test' autoPlay infiniteLoop interval={3000}>
          <div className='slide'>
            <img className='img' src={require('../images/image1.jpg')} alt="Image 1" />
          </div>
          <div>
            <img src={require('../images/image2.jpg')} alt="Image 2" />
          </div>
          <div>
            <img src={require('../images/image3.jpg')} alt="Image 3" />
          </div>
         
        </Carousel>
        <div className="description">
          <p className="disc">Until you value yourself, you will not value your time. Until you value your time, you will not do anything with it. -M. Scott Peck
          </p>
        </div>
      </div>
      <GenericSnackbar
        message={snackbarMessage}
        severity={"error"}
        onClose={handleSnackbarClose}
        open={snackbarOpen}
        key={snackbarMessage}
      />
    </div>
  );
};

export default Login;