import callApi from "../api-client/ApiClient";

export const callLoginApi = async (username: string, password: string) => {
  try {
    const response = await callApi<{
      message: any;
      statusCode: number; token: string 
}>('/user/login', {
      method: 'POST',
      body: {
        username,
        password,
      },
    }, false); // Set includeAuthToken to false
    console.log('test response', response);
    return response;
  } catch (error) {
    throw error;
  }
};
